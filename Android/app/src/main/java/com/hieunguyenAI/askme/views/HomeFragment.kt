package com.hieunguyenAI.askme.views

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.hieunguyenAI.askme.viewmodels.HomeViewModel
import com.hieunguyenAI.askme.R
import com.hieunguyenAI.askme.adapter.FeedAdapter
import com.hieunguyenAI.askme.models.Constants
import com.hieunguyenAI.askme.models.Reaction
import com.hieunguyenAI.askme.models.ReactionData
import com.hieunguyenAI.askme.databinding.ListLayoutBinding
import com.hieunguyenAI.askme.di.ViewModelProviderFactory
import com.hieunguyenAI.askme.extensions.gone
import com.hieunguyenAI.askme.extensions.openFragmentInto
import com.hieunguyenAI.askme.extensions.show
import com.hieunguyenAI.askme.extensions.str
import com.hieunguyenAI.askme.utils.Session
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class HomeFragment : DaggerFragment() {

    @Inject lateinit var mFeedAdapter : FeedAdapter
    private lateinit var mHomeViewModel : HomeViewModel
    private lateinit var mListLayoutBinding: ListLayoutBinding
    @Inject lateinit var providerFactory : ViewModelProviderFactory

    private val LOG_TAG = "HomeFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mListLayoutBinding = DataBindingUtil.inflate(inflater,R.layout.list_layout, container, false)
        mHomeViewModel = ViewModelProviders.of(this, providerFactory).get(HomeViewModel::class.java)

        setupUserList()

        mHomeViewModel.loadUserHomeFeed(Session.getUserId(context!!).str())

        mListLayoutBinding.loadingBar.show()

        mHomeViewModel.getFeedPagedList().observe(this, Observer {
            mFeedAdapter.submitList(it)
            mListLayoutBinding.loadingBar.gone()
        })

        return mListLayoutBinding.root
    }

    private fun setupUserList(){
        mListLayoutBinding.listItems.setHasFixedSize(true)
        mListLayoutBinding.listItems.layoutManager = LinearLayoutManager(context)
        mListLayoutBinding.listItems.adapter = mFeedAdapter

        mFeedAdapter.setOnUsernameListener(object : FeedAdapter.OnUsernameClick {
            override fun onUserClick(userId: String) {
                val profileFragment = ProfileFragment()

                val args = Bundle()
                args.putString(Constants.USER_ID, userId)
                profileFragment.arguments = args

                fragmentManager?.openFragmentInto(R.id.viewContainers, profileFragment)
            }
        })

        mFeedAdapter.setOnReactionListener(object : FeedAdapter.OnReactionClick {
            override fun onReactClick(answerId: Int, toUser : String, reaction: Reaction, callback: FeedAdapter.Callback){
                when(reaction){
                    Reaction.REACATED -> {
                        val token = Session.getHeaderToken(context!!).str()
                        val id =  Session.getUserId(context!!).str()
                        val body = ReactionData(id, toUser, answerId.str())

                        mHomeViewModel.unreactAnswer(token, body, callback)
                    }
                    Reaction.UN_REACATED -> {
                        val token = Session.getHeaderToken(context!!).str()
                        val id =  Session.getUserId(context!!).str()
                        val body = ReactionData(id, toUser, answerId.str())

                        mHomeViewModel.reactAnswer(token, body, callback)
                    }
                }
            }
        })
    }
}