package com.hieunguyenAI.askme.utils

interface NetworkStateListener {
    fun onNetworkConnected()
    fun onNetworkDisconnected()
}