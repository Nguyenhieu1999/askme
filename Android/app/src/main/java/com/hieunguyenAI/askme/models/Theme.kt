package com.hieunguyenAI.askme.models

import androidx.annotation.ColorRes
import androidx.annotation.StyleRes

class Theme(
    @ColorRes val colorValue : Int,
    @StyleRes val colorStyle : Int,
    val themeColor : ThemeColor
)