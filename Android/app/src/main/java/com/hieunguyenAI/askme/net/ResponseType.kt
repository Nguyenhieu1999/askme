package com.hieunguyenAI.askme.net

enum class ResponseType {
    SUCCESS,
    FAILURE,
    NO_AUTH
}