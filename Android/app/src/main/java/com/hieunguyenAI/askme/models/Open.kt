package com.hieunguyenAI.askme.models

import com.google.gson.annotations.SerializedName

enum class Open {
    @SerializedName("1")
    OPENED,

    @SerializedName("0")
    UN_OPENED
}