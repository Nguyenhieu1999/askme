package com.hieunguyenAI.askme.utils

import com.hieunguyenAI.askme.R
import com.hieunguyenAI.askme.models.ThemeColor

object ThemeFactory {

    fun getThemeStyle(themeColor: ThemeColor) : Int{
        return when(themeColor){
            ThemeColor.BLACK -> R.style.BlackTheme
            ThemeColor.ORANGE -> R.style.AppTheme
            ThemeColor.BLUE -> R.style.BlueTheme
            ThemeColor.RED -> R.style.RedTheme
            ThemeColor.GREEN -> R.style.GreenTheme
            ThemeColor.PURPLE -> R.style.PurpleTheme
        }
    }
}