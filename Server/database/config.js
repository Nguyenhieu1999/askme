const mysql = require('mysql');
const schema = require('../database/schema');

const databaseConfig = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'askme',
};

const connection = mysql.createConnection(databaseConfig);

connection.connect(error => {
    if (error) throw error;

    console.debug("Kết nối đến MySQL thành công!!");

    //Tạo bảng user
    connection.query(schema.MYSQL_USERS_TABLE, (err, res) => {
        if (err) throw err;
        console.log("Đã tạo bảng User!");
    });

    //Tạo bảng question
    connection.query(schema.MYSQL_QUESTIONS_TABLE, (err, res) => {
        if (err) throw err;
        console.log("Đã tạo bảng Questions!");
    });

    //Tạo bảng Answers
    connection.query(schema.MYSQL_ANSWERD_TABLE, (err, res) => {
        if (err) throw err;
        console.log("Đã tạo bảng Answers!");
    });

    //Tạo bảng Follow
    connection.query(schema.MYSQL_FOLLOWS_TABLE, (err, res) => {
        if (err) throw err;
        console.log("Đã tạo bảng Follows!");
    });

    //Tạo bảng thông báo
    connection.query(schema.MYSQL_NOTIFICATIONS_TABLE, (err, res) => {
        if (err) throw err;
        console.log("Đã tạo bảng Notifications!");
    });

    //Tạo bảng reaction
    connection.query(schema.MYSQL_REACTIONS_TABLE, (err, res) => {
        if (err) throw err;
        console.log("Đã tạo bảng Reactions!");
    });
});

module.exports = connection;